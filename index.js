db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);



// [Section] MongoDb Aggregation

	/*
		- MongoDB Aggregation is used to manipulate data and perform operations to create filtered results that helps in analyzing data.
		- Compared to doing CRUD operations, aggreation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisiions w/o haning to cretate frontend application.
	*/

	// using AGGREGATE NETHOD

		/*
			$snatch - used to pass the documents that meet the specified contdition(s) to the next pipepline stage/aggregation process.

			syntax:
				{$match: {field: valueA}}
		*/

			db.fruits.aggregate([
				{$match: {onSale: true}}
			]);


		/*
			$group - used to group elements together and field-value, using the data from the group elements.

			syntax:
				{$group: {_id: "valueA", fieldResult: "valueResultA"}}

		*/

			db.fruits.aggregate([
				{$group: {_id: "$supplier_id", 
				total: {$sum: "$stock"}}}
			]);



		// Aggregate Documents using 2 pipeline stages

		/*
			syntax:
				db.collectionName.aggregate([
					{$match: {onSale: true}},
					{$group: {_id: "$supplier_id", 
						total: {$sum: "$stock"}}}
				]);
		*/

			db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", 
					total: {$sum: "$stock"}}}
			]);


// [Section] Field Projection with aggregation
	/*
		$project - can be used when aggregating data to include/exclude fields from the returned results

		syntax:
			{$project: {field: 1/0}}

		- a field that is set to zero(0) will be excluded while
		- a field that is set to one(1) will be the only one included
	*/


		db.fruits.aggregate([
			{$project: {_id: 0}}
		])


		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", 
				total: {$sum: "$stock"}}},
			{$project: {_id: 0}}
		]);


// [Section] Sorting Aggregated Results
	/*
		$sort - can be used to change the order of the aggregated results.
		 - providing a value of -1 will sort the aggregated results in a REVERSE ORDER

		syntax:
			${$sort:{field: 1/-1}}
	*/

		db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$supplier_id", 
				total: {$sum: "$stock"}}},
			{$sort:{total: -1}}
		]);

	// the field that is set to positive Val (1) was sorted in ascending.
	// the field that is set to negative Val (-1) was sorted in descending.


// [Section] Aggregating results based on the array fields
	/*
		"$unwind" - deconstructs an array field from a collection w/ the an array value to give us a result for each array element.

		syntax:
			{$unwind: field}
	*/

		db.fruits.aggregate([
			{$unwind: "$origin"}
		])